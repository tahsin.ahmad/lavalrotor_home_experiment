import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_grundig.json"  # path to my setup file
measure_duration_in_s = 20 # runtime of measurement

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
accelerometer_uuid = "1ee847be-fddd-6ee4-892a-68c4555b0981"  # my accelerometer uuid
# initializing data structure for x-y-z and timestamp
data = {
    accelerometer_uuid: {
        'acceleration_x': [],
        'acceleration_y': [],
        'acceleration_z': [],
        'timestamp': []
    }
}
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
start_time=time.time()
while time.time() - start_time < measure_duration_in_s:
    acceleration = accelerometer.acceleration
    timestamp = time.time() - start_time  # Calculating the timestamp
    data[accelerometer_uuid]["acceleration_x"].append(acceleration[0])
    data[accelerometer_uuid]["acceleration_y"].append(acceleration[1])
    data[accelerometer_uuid]["acceleration_z"].append(acceleration[2])
    data[accelerometer_uuid]["timestamp"].append(timestamp)
    time.sleep(0.001) # "Nach jedem Schleifendurchlauf soll das Programm 1 ms pausieren."
    print(acceleration)
# ---------------------------------------------------------------------------------------------#3-end

 
# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# creating h5 file with respective attributes
data_array = np.array(data[accelerometer_uuid])
with h5py.File(path_h5_file, "w") as h5_file:
    # x dataset
    acceleration_x_dataset=h5_file.create_dataset("acceleration_x", data=data[accelerometer_uuid]["acceleration_x"])
    acceleration_x_dataset.attrs['unit'] = 'm/s^2' 
    # y dataset
    acceleration_y_dataset=h5_file.create_dataset("acceleration_y", data=data[accelerometer_uuid]["acceleration_y"])
    acceleration_y_dataset.attrs['unit'] = 'm/s^2' 
    # z dataset
    acceleration_z_dataset=h5_file.create_dataset("acceleration_z", data=data[accelerometer_uuid]["acceleration_z"])
    acceleration_z_dataset.attrs['unit'] = 'm/s^2' 
    #timestamp
    timestamp_dataset=h5_file.create_dataset("timestamp", data=data[accelerometer_uuid]["timestamp"])
    timestamp_dataset.attrs['unit'] = 's'
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
